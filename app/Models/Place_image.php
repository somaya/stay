<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Place_image extends Model
{
    use HasFactory;
    protected $table = 'places_images';
    protected $guarded = [];
    public $timestamps = true;
}
