<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Place extends Model
{
    use HasFactory;
    protected $table = 'places';
    protected $guarded = [];
    public $timestamps = true;

    public function images()
    {
        return $this->hasMany(Place_image::class, 'place_id');
    }

}
