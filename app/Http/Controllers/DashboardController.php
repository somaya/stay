<?php

namespace App\Http\Controllers;

use App\Models\Booking;
use App\Models\Service;
use App\Models\User;
use Illuminate\Http\Request;


class DashboardController extends Controller
{
    public function index(){

        $users = User::where('role',3)->count();
        $services = Service::count();
        $suppliers = User::where('role',2)->count();


        $bookings = Booking::whereHas('service', function ($q){
            $q->where('category_id',auth()->user()->category_id)
                ->where('user_id',auth()->id());
        })->count();

        return view('admin.dashboard',compact('services','suppliers','bookings','users'));
    }
}
