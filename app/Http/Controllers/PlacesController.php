<?php

namespace App\Http\Controllers;

use App\Models\Place;
use Illuminate\Http\Request;

class PlacesController extends Controller
{
    public function index(){
        $places=Place::all();
        return view('website.places.index',compact('places'));

    }
    public function show($id){
        $place=Place::find($id);
        return view('website.places.show',compact('place'));

    }
}
