<?php

namespace App\Http\Controllers;

use App\Models\Booking;
use App\Models\Category;
use App\Models\Service;
use Illuminate\Http\Request;

class ServicesController extends Controller
{
    public function index($name){
        $category=Category::where('name',$name)->first();
        $services=Service::where('category_id',$category->id)->get();
        return view('website.services.index',compact('category','services'));

    }
    public function show($name,$id){
        $category=Category::where('name',$name)->first();
        $service=Service::find($id);
        return view('website.services.show',compact('category','service'));

    }
    public function showBookingForm($name,$id){
        $category=Category::where('name',$name)->first();
        $service=Service::find($id);
        return view('website.services.booking',compact('category','service'));

    }
    public function storeBooking($name,$id,Request $request){
        $category=Category::where('name',$name)->first();
        $service=Service::find($id);
        $this->validate($request, [
            'date' => 'required',
        ]);
        if (in_array($category->id, [1,3,4])){
            $this->validate($request, [
                'time' => 'required',
            ]);
        }
        Booking::create([
            'user_id'=>auth()->id(),
            'service_id'=>$service->id,
            'date'=>$request->date,
            'time'=>$request->time,
            'delivary'=>$request->has('delivary'),

        ]);
        return redirect()->back()->with('success','Booking Sent Successfully');



    }
}
