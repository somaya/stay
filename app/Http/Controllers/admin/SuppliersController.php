<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use App\Models\Category;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class SuppliersController extends Controller
{
    public function index(){
        $suppliers = User::where('role', 2)->orderBy('created_at','asc')->get();
        return view('admin.suppliers.index',compact('suppliers'));
    }
    public function create(){
        $catgories=Category::all();
        return view('admin.suppliers.single',compact('catgories'));
    }
    public function store(Request $request){
        $this->validate($request, [
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:8'],
            'category' => ['required'],
        ]);

        User::create([
            'name' => $request->name,
            'email' => $request->email,
            'password' => Hash::make($request->password),
            'role' => 2,//supplier
            'approve' => 0,
            'category_id' => $request->category,
        ]);
        return redirect('/admin/suppliers')->with('success','Supplier Added Successfully');

    }
    public function edit($id){
        $user=User::find($id);
        $catgories=Category::all();
        return view('admin.suppliers.single',compact('user','catgories'));
    }
    public function update($id,Request $request)
    {
        $this->validate($request, [
            'name' => ['required', 'string', 'max:255'],
            'category' => ['required'],
        ]);
        $user = User::where('id', $id)->first();
        $user->update([
            'name' => $request->name,
            'category_id' => $request->category,
        ]);
        if ($request->email != $user->email) {
            $this->validate($request, [
                'email' => 'email|unique:users',
            ]);
            $user->update([
                'email' => $request->email
            ]);
        }

        if ($request->password != '') {
            $user->update([
                'password' => Hash::make($request->password),
            ]);
        }
        return redirect('/admin/suppliers')->with('success','Supplier Edited Successfully');


    }
    public function approve($id){
        $user=User::find($id);
        $user->update(['approve'=>1]);
        return redirect('/admin/suppliers')->with('success','Supplier Approved Successfully');
    }
    public function reject($id){
        $user=User::find($id);
        $user->delete();
        return redirect('/admin/suppliers')->with('success','Supplier Rejected Successfully');
    }

    public function destroy($id){
        User::destroy($id);

    }
}
