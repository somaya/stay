<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use App\Models\Place;
use App\Models\Place_image;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class PlacesController extends Controller
{
    public function index(){
        $places = Place::all();
        return view('admin.places.index',compact('places'));
    }
    public function create(){
        return view('admin.places.single');
    }
    public function store(Request $request){
        $this->validate($request, [
            'name' => ['required', 'string', 'max:255'],
            'address' => ['required'],
            'info' => ['required'],
            'images' => 'required'

        ]);


        $place= Place::create([
            'name' => $request->name,
            'address' => $request->address,
            'latitude' => $request->latitude,
            'langitude' => $request->langitude,
            'info' => $request->info,

        ]);
        if ($request->hasFile('images'))
        {
            $request->validate([

                'images.*' => 'image|mimes:jpeg,png,jpg,gif,svg'
            ]);

            $files=$request->file('images');
            foreach ($files as $file)
            {
                $imageName = Str::random(10) . '.' . $file->getClientOriginalExtension();
                $file->move(
                    base_path() . '/public/uploads/places/', $imageName
                );
                Place_image::create([
                    'place_id'=>$place->id,
                    'image'=>'/uploads/places/' . $imageName
                ]);

            }

        }

        return redirect('/admin/places')->with('success','Added Successfully');

    }
    public function edit($id){
        $place=Place::find($id);
        return view('admin.places.single',compact('place'));
    }
    public function update($id,Request $request)
    {
        $this->validate($request, [
            'name' => ['required', 'string', 'max:255'],
            'address' => ['required'],
            'info' => ['required'],

        ]);

        $place=Place::find($id);

        $place->update([
            'name' => $request->name,
            'address' => $request->address,
            'latitude' => $request->latitude,
            'langitude' => $request->langitude,
            'info' => $request->info,


        ]);
        if ($request->hasFile('images'))
        {
            $request->validate([

                'images.*' => 'image|mimes:jpeg,png,jpg,gif,svg'
            ]);

            $files=$request->file('images');
            foreach ($files as $file)
            {
                $imageName = Str::random(10) . '.' . $file->getClientOriginalExtension();
                $file->move(
                    base_path() . '/public/uploads/places/', $imageName
                );
                Place_image::create([
                    'place_id'=>$place->id,
                    'image'=>'/uploads/places/' . $imageName
                ]);

            }

        }

        return redirect('/admin/places')->with('success','Edited Successfully');

    }

    public function destroy($id){
        Place::destroy($id);

    }
    public function image_delete(Request $request){

        Place_image::find($request->image_id)->delete();

        return response()->json('1');

    }

}
