<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class UsersController extends Controller
{
    public function index(){
        $users = User::where('role', 3)->orderBy('created_at','asc')->get();
        return view('admin.users.index',compact('users'));
    }
    public function create(){
        return view('admin.users.single');
    }
    public function store(Request $request){
        $this->validate($request, [
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:8'],
        ]);

        User::create([
            'name' => $request->name,
            'email' => $request->email,
            'password' => Hash::make($request->password),
            'role' => 3,//user
        ]);
        return redirect('/admin/users')->with('success','User Added Successfully');

    }
    public function edit($id){
        $user=User::find($id);
        return view('admin.users.single',compact('user'));
    }
    public function update($id,Request $request)
    {
        $this->validate($request, [
            'name' => ['required', 'string', 'max:255'],
        ]);
        $user = User::where('id', $id)->first();
        $user->update([
            'name' => $request->name,
        ]);
        if ($request->email != $user->email) {
            $this->validate($request, [
                'email' => 'email|unique:users',
            ]);
            $user->update([
                'email' => $request->email
            ]);
        }

        if ($request->password != '') {
            $user->update([
                'password' => Hash::make($request->password),
            ]);
        }
        return redirect('/admin/users')->with('success','User Edited Successfully');


    }

    public function destroy($id){
        User::destroy($id);

    }

}
