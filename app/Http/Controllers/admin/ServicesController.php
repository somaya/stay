<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use App\Models\Service;
use App\Models\Service_image;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class ServicesController extends Controller
{
    public function index(){
        $services = Service::where('category_id',auth()->user()->category_id)->where('user_id',auth()->id())->get();
        return view('admin.services.index',compact('services'));
    }
    public function create(){
        return view('admin.services.single');
    }
    public function store(Request $request){
        $this->validate($request, [
            'name' => ['required', 'string', 'max:255'],
            'phone' => ['required'],
            'price' => ['required'],
            'images' => 'required'

        ]);
        if (auth()->user()->category->id==1){
            $this->validate($request, [
                'meals' => ['required'],
                'address' => ['required'],
            ]);
        }
        if (auth()->user()->category->id==3){
            $this->validate($request, [
                'speciality' => ['required'],
                'address' => ['required'],
            ]);
        }
        if (auth()->user()->category->id==4){
            $this->validate($request, [
                'car_type' => ['required'],
            ]);
        }
        if (auth()->user()->category->id==2){
            $this->validate($request, [
                'room_type' => ['required'],
                'address' => ['required'],
            ]);
        }

       $service= Service::create([
            'name' => $request->name,
            'phone' => $request->phone,
            'price' => $request->price,
            'address' => $request->address,
            'latitude' => $request->latitude,
            'langitude' => $request->langitude,
            'meals' => $request->meals,
            'car_type' => $request->car_type,
            'room_type' => $request->room_type,
            'speciality' => $request->speciality,
            'category_id' => auth()->user()->category->id,
            'user_id' => auth()->id(),
        ]);
        if ($request->hasFile('images'))
        {
            $request->validate([

                'images.*' => 'image|mimes:jpeg,png,jpg,gif,svg'
            ]);

            $files=$request->file('images');
            foreach ($files as $file)
            {
                $imageName = Str::random(10) . '.' . $file->getClientOriginalExtension();
                $file->move(
                    base_path() . '/public/uploads/services/', $imageName
                );
                Service_image::create([
                    'service_id'=>$service->id,
                    'image'=>'/uploads/services/' . $imageName
                ]);

            }

        }

        return redirect('/admin/services')->with('success','Added Successfully');

    }
    public function edit($id){
        $service=Service::find($id);
        return view('admin.services.single',compact('service'));
    }
    public function update($id,Request $request)
    {
        $this->validate($request, [
            'name' => ['required', 'string', 'max:255'],
            'phone' => ['required'],
            'price' => ['required'],

        ]);
        if (auth()->user()->category->id==1){
            $this->validate($request, [
                'meals' => ['required'],
            ]);
        }
        if (auth()->user()->category->id==3){
            $this->validate($request, [
                'speciality' => ['required'],
            ]);
        }
        if (auth()->user()->category->id==4){
            $this->validate($request, [
                'car_type' => ['required'],
            ]);
        }
        if (auth()->user()->category->id==2){
            $this->validate($request, [
                'room_type' => ['required'],
            ]);
        }
        $service=Service::find($id);

        $service->update([
            'name' => $request->name,
            'phone' => $request->phone,
            'price' => $request->price,
            'address' => $request->address,
            'latitude' => $request->latitude,
            'langitude' => $request->langitude,
            'meals' => $request->meals,
            'car_type' => $request->car_type,
            'room_type' => $request->room_type,
            'speciality' => $request->speciality,

        ]);
        if ($request->hasFile('images'))
        {
            $request->validate([

                'images.*' => 'image|mimes:jpeg,png,jpg,gif,svg'
            ]);

            $files=$request->file('images');
            foreach ($files as $file)
            {
                $imageName = Str::random(10) . '.' . $file->getClientOriginalExtension();
                $file->move(
                    base_path() . '/public/uploads/services/', $imageName
                );
                Service_image::create([
                    'service_id'=>$service->id,
                    'image'=>'/uploads/services/' . $imageName
                ]);

            }

        }

        return redirect('/admin/services')->with('success','Edited Successfully');

    }

    public function destroy($id){
        Service::destroy($id);

    }
    public function image_delete(Request $request){

        Service_image::find($request->image_id)->delete();

        return response()->json('1');

    }

}
