<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use App\Models\Booking;
use Illuminate\Http\Request;

class BookingsController extends Controller
{
    public function index(){
        $bookings = Booking::whereHas('service', function ($q){
            $q->where('category_id',auth()->user()->category_id)
                ->where('user_id',auth()->id());
        })->orderBy('created_at','asc')->get();
        return view('admin.bookings.index',compact('bookings'));
    }
    public function show($id){
        $booking=Booking::find($id);
        return view('admin.bookings.show',compact('booking'));
    }
}
