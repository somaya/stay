@extends('website.layouts.app')

@section('content')
    <div class="undermenuarea">
        <div class="boxedshadow">
        </div>
        <div class="grid">
            <div class="row">
                <div class="c8">
                    <h1 class="titlehead">{{$category->name}}</h1>
                </div>

            </div>
        </div>
    </div>
    <!-- CONTENT
    ================================================== -->
    <div class="grid">
        <div class="shadowundertop"></div>


        <!-- end filter -->
        <div class="row space-top">
            <div id="content">
                <!-- image 1 -->

                @foreach($services as $service)
                    <div class="boxfourcolumns cat1 cat3">
                        <div class="boxcontainer">
						<span class="gallery">
						<a data-gal="prettyPhoto[gallery1]" href="{{$service->images->isNotEmpty()?$service->images[0]->image:''}}"><img width="200px" height="200px" src="{{$service->images->isNotEmpty()?$service->images[0]->image:''}}" alt="Add Title" class="imgOpa"/></a>
						</span>
                            <h1><a href="/services/{{$category->name}}/{{$service->id}}">{{$service->name}}</a></h1>
                            <p>
                                {{$service->address}}
                            </p>
                        </div>
                    </div>
                @endforeach


            </div>
        </div>
    </div>
@endsection