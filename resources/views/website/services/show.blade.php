@extends('website.layouts.app')
@section('content')
    <div class="undermenuarea">
        <div class="boxedshadow">
        </div>
        <div class="grid">
            <div class="row">
                <div class="c8">
                    <h1 class="titlehead">{{$service->name}}</h1>
                </div>
                <div class="c4">
                    <h1 class="titlehead rightareaheader"><i class="icon-map-marker"></i> Call Us Now {{$service->phone}}</h1>
                </div>
            </div>
        </div>
    </div>
    <!-- CONTENT
    ================================================== -->
    <div class="grid">
        <div class="shadowundertop">
            @include('message')
        </div>
        <div class="row">
            <div class="c12">
                <h1 class="maintitle ">
                    <span>{{$service->name}}</span>
                </h1>
            </div>
        </div>
        <div class="row">

            <!-- begin description area -->
            <div class="c6">
                <p>
                    @if($category->name=='Restaurants')
                        {{$service->meals}}
                    @endif
                </p>
                <div id="service_map" lat="{{$service->latitude}}" lng="{{$service->langitude}}" style="width: 100%; height: 400px;"></div>
                <p>Phone : {{$service->phone}}<br>
                Supplier : {{$service->user->name}}<br>
                Price : {{$service->price}}<br>
                Category : {{$category->name}}<br>

                @if($category->name=='Transportation')
                Car Type : {{$service->car_type}}<br>
                @endif
                @if($category->name=='Medical')
                Speciality : {{$service->speciality}}<br>
                @endif
                @if($category->name=='Hotels')
                Room Type : {{$service->room_type}}</p><br>
                @endif

            </div><!-- end description area -->


            @if($service->images->isNotEmpty())
                <div class="c6">
                    <div class="flexslider">
                        <ul class="slides">
                            @foreach($service->images as $image)
                                <li>
                                    <img src="{{$image->image}}" alt="">
                                </li>
                            @endforeach

                        </ul>
                    </div>
                </div>
            @endif


        </div>
    </div>
    <div>

        <button class="large red"><a href="/booking/{{$category->name}}/{{$service->id}}">Booking </a></button>
    </div>
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDaFbSOerPJ0NF5IArDBvQ_dX3ODWnln5c&language=en&libraries=places&callback=initialize"
            async defer></script>
    <script>
        /* script */
        function initialize() {
            initServiceMap();
        }
        function initServiceMap() {

            var m=document.getElementById('service_map');
            var lng= m.getAttribute('lng');
            var lat= m.getAttribute('lat');
            var latlng = new google.maps.LatLng(lat,lng);
            var map = new google.maps.Map(document.getElementById('service_map'), {
                center: latlng,
                zoom: 13
            });
            var marker = new google.maps.Marker({
                map: map,
                position: latlng,
//                draggable: true,
                anchorPoint: new google.maps.Point(0, -29)
            });

        }

    </script>
@endsection