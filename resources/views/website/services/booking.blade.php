@extends('website.layouts.app')

@section('content')
    <div class="undermenuarea">
        <div class="boxedshadow">
        </div>
        <div class="grid">
            <div class="row">
                <div class="c8">
                    <h1 class="titlehead">Booking</h1>
                </div>
                <div class="c4">
                    <h1 class="titlehead rightareaheader"><i class="icon-map-marker"></i> Call Us Now 1-318-107-432</h1>
                </div>
            </div>
        </div>
    </div>
    <!-- CONTENT
    ================================================== -->
    <div class="container">
        @include('message')
        <div class="grid justify-content-center ">
            <div class="row">
                <div class="c6">
                    <h3>Booking</h3>

                    <div class="card-body">
                        <form method="POST" action="/booking/{{$category->name}}/{{$service->id}}">
                            @csrf


                            <div class="form-group row">
                                <label for="date" class="col-md-4 col-form-label text-md-right">{{ __('Date') }}</label>

                                <div class="col-md-6">
                                    <input id="date" type="date" class="form-control @error('date') is-invalid @enderror" name="date" value="{{ old('date') }}" required autocomplete="date" >

                                    @error('date')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>

                            @if(in_array($category->id, [1,3,4]))
                                {{--if category restaurant ,medical,transportation--}}

                                <div class="form-group row">
                                <label for="time" class="col-md-4 col-form-label text-md-right">{{ __('Time') }}</label>

                                <div class="col-md-6">
                                    <input id="datetimepicker1" type="time" class="form-control date @error('time') is-invalid @enderror" name="time" value="{{ old('time') }}" required autocomplete="time" >

                                    @error('time')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>
                            @endif
                            @if($category->id==1)
                                {{--if category restaurant--}}


                            <div class="form-group row">
                                <label for="delivary" class="col-md-4 col-form-label text-md-right">{{ __('Delivary') }}</label>

                                <div class="col-md-6">
                                    <input id="delivary" type="checkbox" class="form-control @error('delivary') is-invalid @enderror" name="delivary" >

                                    @error('delivary')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>
                            @endif



                            <div class="form-group row mb-0">
                                <div class="col-md-8 offset-md-4">
                                    <button type="submit" class="skyblue">
                                        {{ __('Submit') }}
                                    </button>


                                </div>
                            </div>
                        </form>
                    </div>
                </div>

            </div>
        </div>
    </div>
    <script>
        $('#datetimepicker1').datetimepicker({
            format: 'hh:mm:ss'
        });
    </script>
@endsection
