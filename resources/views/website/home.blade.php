@extends('website.layouts.app')

@section('content')
<div class="undermenuarea">
    <div class="boxedshadow">
    </div>
    <!-- SLIDER AREA
    ================================================== -->
    <div id="da-slider" class="da-slider">
        <!--Slide 1-->
        <div class="da-slide">
            <h2> STAY Introduction </h2>
            <p>
                This project aims to help those coming to the city of Tabuk and provide them with all the basic needs such as hospitals and restaurants, as well as transportation and daily weather conditions, and to be the first reference for all those coming to this wonderful city..
            </p>
            <a href="#" class="da-link" style="width:202px;">Read More</a>
            <div class="da-img">
                <img src="{{ Request::root() }}/website/images/001.jpg" alt="">
            </div>
        </div>
        <!--Slide 2-->
        <div class="da-slide">
            <h2>Problem Overview</h2>
            <p>
                Many people face problems when moving to a new area for the purpose of studying or working, including finding the best restaurants, best hotels, and even entertainment venues..
            </p>
            <a href="#" class="da-link" style="width:192px;">Read More</a>
            <div class="da-img">
                <img src="{{ Request::root() }}/website/images/002.jpg" alt="">
            </div>
        </div>
        <!--Slide 3-->
        <div class="da-slide">
            <h2> Tabouk city </h2>
            <p>
                Tabouk, is the capital city of the Tabuk Region in northwestern Saudi Arabia. It has a population of 667,000 (as of 2021). It is close to the Jordanian–Saudi Arabia border, and houses the largest air force base in Saudi Arabia.
            </p>
            <a href="#" class="da-link" style="width:230px;">Read More</a>
            <div class="da-img">
                <img src="{{ Request::root() }}/website/images/003.jpg" alt="">
            </div>
        </div>
        <nav class="da-arrows">
            <span class="da-arrows-prev"></span>
            <span class="da-arrows-next"></span>
        </nav>
    </div>
</div>
<!-- UNDER SLIDER - BLACK AREA
================================================== -->
<div class="undersliderblack">
    <div class="grid">
        <div class="row space-bot">
            <div class="c12">
                <!--Box 1-->
                <div class="c4 introbox introboxfirst">
                    <div class="introboxinner">
						<span class="homeicone">
						<i class="icon-bolt"></i>
						</span> STAY website will include All These services All in one To make it Easy for expatriate or find all means of suitable services.
                    </div>
                </div>
                <!--Box 2-->
                <div class="c4 introbox introboxmiddle">
                    <div class="introboxinner">
						<span class="homeicone">
						<i class="icon-cog"></i>
						</span> The Emirate of Tabuk Is one of the largest cities in northern Saudi Arabia, there are the most important monuments..
                    </div>
                </div>
                <!--Box 3-->
                <div class="c4 introbox introboxlast">
                    <div class="introboxinner">
						<span class="homeicone">
						<i class="icon-lightbulb"></i>
						</span>
                        This city is considered the northern gateway to the Arabian Peninsula. A Research has been made in 2020 .
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="shadowunderslider">
</div>
<div class="grid">
    <div class="row space-bot">
        <!--INTRO-->
        <div class="c12">
            <div class="royalcontent">
                <h1 class="royalheader">WELCOME TO STAY |  مُكث, WEBSITE</h1>
                <h1 class="title" style="text-transform:none;">This project aims to help those coming to Tabuk city and provide them with all basic needs and to be the first reference for all those coming to this wonderful city.</h1>
            </div>
        </div>
        <!--Box 1-->
        <div class="c4">
            <h2 class="title hometitlebg"><i class="icon-qrcode smallrightmargin"></i> Tourist Places</h2>
            <div class="noshadowbox">
                <h5>Tourism</h5>
                <p>
                    Tourism media is one of the most important elements of the tourism marketing mix because it has a role in promoting and defining the country's tourism sites.
                </p>
                <p class="bottomlink">
                    <a href="#" class="neutralbutton"><i class="icon-link"></i></a>
                </p>
            </div>
        </div>
        <!--Box 2-->
        <div class="c4">
            <h2 class="title hometitlebg"><i class="icon-cog smallrightmargin"></i> Weather Service</h2>
            <div class="noshadowbox">
                <h5>Weather</h5>
                <p>
                    Weather forecast sites are very important for any traveler or expatriate, so it is necessary to provide this service at the expatriate service sites.
                </p>
                <p class="bottomlink">
                    <a href="#" class="neutralbutton"><i class="icon-link"></i></a>
                </p>
            </div>
        </div>
        <!--Box 3-->
        <div class="c4">
            <h2 class="title hometitlebg"><i class="icon-user" style="margin-right:10px;"></i> Services Website</h2>
            <div class="noshadowbox">
                <h5>Services</h5>
                <p>
                    Services have used various methods for making reservations in order to maximize the cover count for their particular Services.
                </p>
                <p class="bottomlink">
                    <a href="#" class="neutralbutton"><i class="icon-link"></i></a>
                </p>
            </div>
        </div>
    </div>
    <!-- SHOWCASE
    ================================================== -->
    <div class="row space-top">
        <div class="c12 space-top">
            <h1 class="maintitle ">
                <span>SHOWCASE</span>
            </h1>
        </div>
    </div>
    <div class="row space-bot">
        <div class="c12">
            <div class="list_carousel">
                <div class="carousel_nav">
                    <a class="prev" id="car_prev" href="#"><span>prev</span></a>
                    <a class="next" id="car_next" href="#"><span>next</span></a>
                </div>
                <div class="clearfix">
                </div>
                <ul id="recent-projects">
                    <!--featured-projects 1-->
                    <li>
                        <div class="featured-projects">
                            <div class="featured-projects-image">
                                <a href="#"><img src="{{ Request::root() }}/website/images/Bslider/1.jpg" class="imgOpa" alt=""></a>
                            </div>
                            <div class="featured-projects-content">
                                <h1><a href="#">Tabuk  City</a></h1>

                            </div>
                        </div>
                    </li>
                    <!--featured-projects 2-->
                    <li>
                        <div class="featured-projects">
                            <div class="featured-projects-image">
                                <a href="#"><img src="{{ Request::root() }}/website/images/Bslider/8.jpg" class="imgOpa" alt=""></a>
                            </div>
                            <div class="featured-projects-content">
                                <h1><a href="#">Tabuk  City </a></h1>

                            </div>
                        </div>
                    </li>
                    <!--featured-projects 3-->
                    <li>
                        <div class="featured-projects">
                            <div class="featured-projects-image">
                                <a href="#"><img src="{{ Request::root() }}/website/images/Bslider/7.jpg" class="imgOpa" alt=""></a>
                            </div>
                            <div class="featured-projects-content">
                                <h1><a href="#">Tabuk  City</a></h1>

                            </div>
                        </div>
                    </li>
                    <!--featured-projects 4-->
                    <li>
                        <div class="featured-projects">
                            <div class="featured-projects-image">
                                <a href="#"><img src="{{ Request::root() }}/website/images/Bslider/6.jpg" class="imgOpa" alt=""></a>
                            </div>
                            <div class="featured-projects-content">
                                <h1><a href="#">Tabuk  City</a></h1>

                            </div>
                        </div>
                    </li>
                    <!--featured-projects 5-->
                    <li>
                        <div class="featured-projects">
                            <div class="featured-projects-image">
                                <a href="#"><img src="{{ Request::root() }}/website/images/Bslider/5.jpg" class="imgOpa" alt=""></a>
                            </div>
                            <div class="featured-projects-content">
                                <h1><a href="#">Tabuk  City</a></h1>

                            </div>
                        </div>
                    </li>
                    <!--featured-projects 6-->
                    <li>
                        <div class="featured-projects">
                            <div class="featured-projects-image">
                                <a href="#"><img src="{{ Request::root() }}/website/images/Bslider/4.jpg" class="imgOpa" alt=""></a>
                            </div>
                            <div class="featured-projects-content">
                                <h1><a href="#">Tabuk  City</a></h1>

                            </div>
                        </div>
                    </li>
                    <!--featured-projects 7-->
                    <li>
                        <div class="featured-projects">
                            <div class="featured-projects-image">
                                <a href="#"><img src="{{ Request::root() }}/website/images/Bslider/3.jpg" class="imgOpa" alt=""></a>
                            </div>
                            <div class="featured-projects-content">
                                <h1><a href="#">Tabuk  City</a></h1>

                            </div>
                        </div>
                    </li>
                    <!--featured-projects 8-->
                    <li>
                        <div class="featured-projects">
                            <div class="featured-projects-image">
                                <a href="#"><img src="{{ Request::root() }}/website/images/Bslider/2.jpg" class="imgOpa" alt=""></a>
                            </div>
                            <div class="featured-projects-content">
                                <h1><a href="#">Tabuk  City</a></h1>

                            </div>
                        </div>
                    </li>
                </ul>
                <div class="clearfix">
                </div>
            </div>
        </div>
    </div>
    <!-- CALL TO ACTION
    ================================================== -->

</div>
@endsection