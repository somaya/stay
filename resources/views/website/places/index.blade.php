@extends('website.layouts.app')

@section('content')
    <div class="undermenuarea">
        <div class="boxedshadow">
        </div>
        <div class="grid">
            <div class="row">
                <div class="c8">
                    <h1 class="titlehead">Tourist Places</h1>
                </div>

            </div>
        </div>
    </div>
    <!-- CONTENT
    ================================================== -->
    <div class="grid">
        <div class="shadowundertop"></div>


        <!-- end filter -->
        <div class="row space-top">
            <div id="content">
                <!-- image 1 -->

                @foreach($places as $place)
                    <div class="boxfourcolumns cat1 cat3">
                        <div class="boxcontainer">
						<span class="gallery">
						<a data-gal="prettyPhoto[gallery1]" href="{{$place->images->isNotEmpty()?$place->images[0]->image:''}}"><img width="200px" height="200px" src="{{$place->images->isNotEmpty()?$place->images[0]->image:''}}" alt="Add Title" class="imgOpa"/></a>
						</span>
                            <h1><a href="/places/{{$place->id}}">{{$place->name}}</a></h1>
                            <p>
                                {{$place->address}}
                            </p>
                        </div>
                    </div>
                @endforeach


            </div>
        </div>
    </div>
@endsection