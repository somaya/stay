@extends('website.layouts.app')
@section('content')
    <div class="undermenuarea">
        <div class="boxedshadow">
        </div>
        <div class="grid">
            <div class="row">
                <div class="c8">
                    <h1 class="titlehead">{{$place->name}}</h1>
                </div>

            </div>
        </div>
    </div>
    <!-- CONTENT
    ================================================== -->
    <div class="grid">
        <div class="shadowundertop">
        </div>
        <div class="row">
            <div class="c12">
                <h1 class="maintitle ">
                    <span>{{$place->name}}</span>
                </h1>
            </div>
        </div>
        <div class="row">

            <!-- begin description area -->
            <div class="c6">
                <p>
                        {{$place->info}}
                </p>
                <div id="place_map" lat="{{$place->latitude}}" lng="{{$place->langitude}}" style="width: 100%; height: 400px;"></div>

            </div><!-- end description area -->

            @if($place->images->isNotEmpty())
                <div class="c6">
                    <div class="flexslider">
                        <ul class="slides">
                            @foreach($place->images as $image)
                                <li>
                                    <img src="{{$image->image}}" alt="">
                                </li>
                            @endforeach

                        </ul>
                    </div>
                </div>
            @endif

        </div>
    </div>
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDaFbSOerPJ0NF5IArDBvQ_dX3ODWnln5c&language=en&libraries=places&callback=initialize"
            async defer></script>
    <script>
        /* script */
        function initialize() {
            initPlaceMap();
        }
        function initPlaceMap() {

            var m=document.getElementById('place_map');
            var lng= m.getAttribute('lng');
            var lat= m.getAttribute('lat');
            var latlng = new google.maps.LatLng(lat,lng);
            var map = new google.maps.Map(document.getElementById('place_map'), {
                center: latlng,
                zoom: 13
            });
            var marker = new google.maps.Marker({
                map: map,
                position: latlng,
//                draggable: true,
                anchorPoint: new google.maps.Point(0, -29)
            });

        }

    </script>
@endsection