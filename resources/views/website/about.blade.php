@extends('website.layouts.app')
@section('content')
    <div class="undermenuarea">
        <div class="boxedshadow">
        </div>
        <div class="grid">
            <div class="row">
                <div class="c8">
                    <h1 class="titlehead">About Us</h1>
                </div>

            </div>
        </div>
    </div>
    <!-- CONTENT
    ================================================== -->
    <div class="grid">
        <div class="shadowundertop"></div>
        <div class="row">
            <div class="c8">
                <h1 class="maintitle ">
                    <span>OUR HISTORY</span>
                </h1>
                <p>
                    <span class="dropcap">I</span> certify that this project report entitled “ STAY | مُكث ” was prepared by STUDENT’S RENAD , STUDENT’S MANAR , STUDENT’S AWATEF, STUDENT’S NJOUD , STUDENT’S AMAL , and STUDENT’S RAZAN has met the required standard for submission in partial fulfilment of the requirements for the award of Bachelor of Computer Science at University of Tabuk.</p>

            </div>

        </div>
        <!-- OUR TEAM
            ================================================== -->
        <div class="row space-top space-bot">
            <div class="c12">
                <h1 class="maintitle text-center">
                    <span>Meet the Team</span>
                </h1>
            </div>
            <!-- box 1 -->
            <div class="c3" style="text-align:center;">
                <img src="http://placehold.it/270x180" class="imgOpa teamimage" alt="">
                <div class="teamdescription">
                    <h1>Renad ALSHARAFAA</h1>
                </div>
            </div>
            <!-- box 2 -->
            <div class="c3">
                <img src="http://placehold.it/270x180" class="imgOpa teamimage" alt="">
                <div class="teamdescription">
                    <h1>Manar ALBALAWI</h1>
                </div>
            </div>
            <!-- box 3 -->
            <div class="c3">
                <img src="http://placehold.it/270x180" class="imgOpa teamimage" alt="">
                <div class="teamdescription">
                    <h1>Awatef ALHARTHI</h1>
                </div>
            </div>
            <!-- box 4 -->
            <div class="c3">
                <img src="http://placehold.it/270x180" class="imgOpa teamimage" alt="">
                <div class="teamdescription">
                    <h1>Njoud ALHAZMI</h1>
                </div>
            </div>
            <div class="c3">
                <img src="http://placehold.it/270x180" class="imgOpa teamimage" alt="">
                <div class="teamdescription">
                    <h1>Amal AAMRI</h1>
                </div>
            </div>
            <div class="c3">
                <img src="http://placehold.it/270x180" class="imgOpa teamimage" alt="">
                <div class="teamdescription">
                    <h1>Razan ASIRI</h1>
                </div>
            </div>
            <div class="c3">
                <img src="http://placehold.it/270x180" class="imgOpa teamimage" alt="">
                <div class="teamdescription">
                    <h1>Dr.A'aeshah ALHAKAMY</h1>
                    <span class="hirefor">Supervisor</span>

                </div>
            </div>


        </div>

    </div>
@endsection