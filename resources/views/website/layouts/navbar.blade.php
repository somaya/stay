<div class="c10">
    <nav id="topNav">
        <ul id="responsivemenu">
            <li class="active"><a href="/"><i class="icon-home homeicon"></i><span class="showmobile">Home</span></a></li>
            <li><a href="#">services</a>
                <ul style="display: none;">
                    @foreach($categories as $category)
                    <li><a href="/services/{{$category->name}}">{{$category->name}}</a></li>
                    @endforeach


                </ul>
            </li>
            <li><a href="/places">Tourist Places</a>

            </li>
            <li><a href="https://weather.com/weather/today/l/28.38,36.57?par=google">Weather</a>

            </li>
            <li class="last"><a href="/about">About</a></li>
            @guest
            <li class="last"><a href="/login">Login</a></li>
            <li class="last"><a href="/register">Register</a></li>
            @else
               
                <li><a href="#" role="button" style="color: blue">{{ Auth::user()->name }} <span class="caret"></span></a>
                    <ul style="display: none;">
                        <li>
                            <a href="{{ route('logout') }}"
                               onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                {{ __('Logout') }}
                            </a>

                            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                @csrf
                            </form>
                        </li>


                    </ul>
                </li>
            @endguest

        </ul>
    </nav>
</div>