<head><meta name="viewport" content="width=device-width"/>
    <title>STAY |  مُكث Website</title>
    <!-- STYLES & JQUERY
    ================================================== -->
    <link rel="stylesheet" type="text/css" href="{{ Request::root() }}/website/css/style.css"/>
    <link rel="stylesheet" type="text/css" href="{{ Request::root() }}/website/css/icons.css"/>
    <link rel="stylesheet" type="text/css" href="{{ Request::root() }}/website/css/slider.css"/>
    <link rel="stylesheet" type="text/css" href="{{ Request::root() }}/website/css/skinorange.css"/><!-- change skin color -->
    <link rel="stylesheet" type="text/css" href="{{ Request::root() }}/website/css/responsive.css"/>
    <script src="{{ Request::root() }}/website/js/jquery-1.9.0.min.js"></script><!-- the rest of the scripts at the bottom of the document -->

</head>