<!DOCTYPE HTML>
<html>

@include('website.layouts.header')
<body>
<div class="boxedtheme">
    <!-- TOP LOGO & MENU
    ================================================== -->
    <div class="grid">
        <div class="row space-bot">
            <!--Logo-->
            <div class="c2">
                <a href="/">
                    <img src="{{ Request::root() }}/website/images/logo.png" class="logo" alt="">
                </a>
            </div>
            <!--Menu-->

            @include('website.layouts.navbar')

        </div>
    </div>

    <!-- START content area
    ================================================== -->

    @yield('content')

    <!-- FOOTER
    ================================================== -->
   @include('website.layouts.footer')

</div>
<!-- JAVASCRIPTS
================================================== -->
<!-- all -->
<script src="{{ Request::root() }}/website/js/modernizr-latest.js"></script>

<!-- menu & scroll to top -->
<script src="{{ Request::root() }}/website/js/common.js"></script>

<!-- slider -->
<script src="{{ Request::root() }}/website/js/jquery.cslider.js"></script>

<!-- cycle -->
<script src="{{ Request::root() }}/website/js/jquery.cycle.js"></script>

<!-- carousel items -->
<script src="{{ Request::root() }}/website/js/jquery.carouFredSel-6.0.3-packed.js"></script>

<!-- twitter -->
<script src="{{ Request::root() }}/website/js/jquery.tweet.js"></script>
<script src="{{ Request::root() }}/website/js/jquery.flexslider-min.js"></script>


<!-- CALL flexslder -->
<script>
    // Can also be used with $(document).ready()
    $(window).load(function() {
        $('.flexslider').flexslider({
            animation: "slide"
        });
    });
</script>

<!-- Call Showcase - change 4 from min:4 and max:4 to the number of items you want visible -->
<script type="text/javascript">
    $(window).load(function(){
        $('#recent-projects').carouFredSel({
            responsive: true,
            width: '100%',
            auto: true,
            circular	: true,
            infinite	: false,
            prev : {
                button		: "#car_prev",
                key			: "left",
            },
            next : {
                button		: "#car_next",
                key			: "right",
            },
            swipe: {
                onMouse: true,
                onTouch: true
            },
            scroll : 2000,
            items: {
                visible: {
                    min: 4,
                    max: 4
                }
            }
        });
    });
</script>

<!-- Call opacity on hover images from carousel-->
<script type="text/javascript">
    $(document).ready(function(){
        $("img.imgOpa").hover(function() {
                $(this).stop().animate({opacity: "0.6"}, 'slow');
            },
            function() {
                $(this).stop().animate({opacity: "1.0"}, 'slow');
            });
    });
</script>
</body>
</html>