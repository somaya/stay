<div id="wrapfooter">
    <div class="grid">
        <div class="row" id="footer">
            <!-- to top button  -->
            <p class="back-top floatright">
                <a href="#top"><span></span></a>
            </p>
            <!-- 1st column -->
            <div class="c3">
                <img src="{{ Request::root() }}/website/images/logo-footer.png" style="padding-top: 70px;" alt="">
            </div>
            <!-- 2nd column -->
            <div class="c3">
                <h2 class="title"><i class="icon-twitter"></i> Follow us</h2>
                <hr class="footerstress">
                <div id="ticker" class="query">
                </div>
            </div>
            <!-- 3rd column -->
            <div class="c3">
                <h2 class="title"><i class="icon-envelope-alt"></i> Contact</h2>
                <hr class="footerstress">
                <dl>
                    <dt>Tabuk University, Project Teamwork </dt>
                    <dd><span>Telephone:</span>+966550947691</dd>
                    <dd>E-mail: <a href="#">Project@gmail.com</a></dd>
                </dl>
                <ul class="social-links" style="margin-top:15px;">
                    <li class="twitter-link smallrightmargin">
                        <a href="#" class="twitter has-tip" target="_blank" title="Follow Us on Twitter">Twitter</a>
                    </li>
                    <li class="facebook-link smallrightmargin">
                        <a href="#" class="facebook has-tip" target="_blank" title="Join us on Facebook">Facebook</a>
                    </li>
                    <li class="google-link smallrightmargin">
                        <a href="#" class="google has-tip" title="Google +" target="_blank">Google</a>
                    </li>
                    <li class="linkedin-link smallrightmargin">
                        <a href="#" class="linkedin has-tip" title="Linkedin" target="_blank">Linkedin</a>
                    </li>
                    <li class="pinterest-link smallrightmargin">
                        <a href="#" class="pinterest has-tip" title="Pinterest" target="_blank">Pinterest</a>
                    </li>
                </ul>
            </div>
            <!-- 4th column -->
            <div class="c3">
                <h2 class="title"><i class="icon-link"></i> Links</h2>
                <hr class="footerstress">
                <ul>
                    @foreach($categories as $category)
                        <li><a href="/services/{{$category->name}}">{{$category->name}}</a></li>
                    @endforeach
                    <li><a href="/places">Tourist Places</a></li>
                    <li><a href="Weather.html">Weather</a></li>
                    <li><a href="/about">About</a></li>

                </ul>
            </div>
            <!-- end 4th column -->
        </div>
    </div>
</div>
<!-- copyright area -->
<div class="copyright">
    <div class="grid">
        <div class="row">
            <div class="c6">
                STAY |  مُكث  &copy; 2022. All Rights Reserved.
            </div>
            <div class="c6">
				<span class="right">
					Renad -	Manar -	Awatef -Njoud -	Amal - Razan  </span>
            </div>
        </div>
    </div>
</div>