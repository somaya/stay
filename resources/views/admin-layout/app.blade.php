<!DOCTYPE html>
<html>

@include('admin-layout.header')

<body class="theme-purple">


@include('admin-layout.navbar')

@include('admin-layout.sidebar')
<section class="content">
    @yield('content')

</section>




<!-- Jquery Core Js -->
<script src="{{ Request::root() }}/admin/plugins/jquery/jquery.min.js"></script>
<script src="{{ Request::root() }}/admin/js/pages/ui/modals.js"></script>

<!-- Bootstrap Core Js -->
<script src="{{ Request::root() }}/admin/plugins/bootstrap/js/bootstrap.js"></script>

<!-- Select Plugin Js -->
<script src="{{ Request::root() }}/admin/plugins/bootstrap-select/js/bootstrap-select.js"></script>

<!-- Slimscroll Plugin Js -->
<script src="{{ Request::root() }}/admin/plugins/jquery-slimscroll/jquery.slimscroll.js"></script>

<!-- Waves Effect Plugin Js -->
<script src="{{ Request::root() }}/admin/plugins/node-waves/waves.js"></script>

<!-- Jquery CountTo Plugin Js -->
<script src="{{ Request::root() }}/admin/plugins/jquery-countto/jquery.countTo.js"></script>

<!-- Morris Plugin Js -->
<script src="{{ Request::root() }}/admin/plugins/raphael/raphael.min.js"></script>
<script src="{{ Request::root() }}/admin/plugins/morrisjs/morris.js"></script>

<!-- Jquery DataTable Plugin Js -->
<script src="{{ Request::root() }}/admin/plugins/jquery-datatable/jquery.dataTables.js"></script>
<script src="{{ Request::root() }}/admin/plugins/jquery-datatable/skin/bootstrap/js/dataTables.bootstrap.js"></script>
<script src="{{ Request::root() }}/admin/plugins/jquery-datatable/extensions/export/dataTables.buttons.min.js"></script>
<script src="{{ Request::root() }}/admin/plugins/jquery-datatable/extensions/export/buttons.flash.min.js"></script>
<script src="{{ Request::root() }}/admin/plugins/jquery-datatable/extensions/export/jszip.min.js"></script>
<script src="{{ Request::root() }}/admin/plugins/jquery-datatable/extensions/export/pdfmake.min.js"></script>
<script src="{{ Request::root() }}/admin/plugins/jquery-datatable/extensions/export/vfs_fonts.js"></script>
<script src="{{ Request::root() }}/admin/plugins/jquery-datatable/extensions/export/buttons.html5.min.js"></script>
<script src="{{ Request::root() }}/admin/plugins/jquery-datatable/extensions/export/buttons.print.min.js"></script>

<!-- Custom Js -->
<script src="{{ Request::root() }}/admin/js/pages/tables/jquery-datatable.js"></script>
<script src="{{ Request::root() }}/adminjs/pages/forms/advanced-form-elements.js"></script>



<!-- Custom Js -->
<script src="{{ Request::root() }}/admin/js/admin.js"></script>
<script src="{{ Request::root() }}/admin/js/pages/index.js"></script>

<!-- Demo Js -->
<script src="{{ Request::root() }}/admin/js/demo.js"></script>
<script src="{{ Request::root() }}/admin/plugins/dropzone/dropzone.js"></script>



</body>

</html>
