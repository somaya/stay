<section>
    <!-- Left Sidebar -->
    <aside id="leftsidebar" class="sidebar">
        <!-- User Info -->
        <div class="user-info">
            <div class="image">
                <img src="{{ Request::root() }}/admin/images/user.png" width="48" height="48" alt="User" />
            </div>
            <div class="info-container">
                <div class="name" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">{{auth()->user()->name}}</div>
                <div class="email">{{auth()->user()->email}}</div>
                <div class="btn-group user-helper-dropdown">
                    <i class="material-icons" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">keyboard_arrow_down</i>
                    <ul class="dropdown-menu pull-right">

                        <li><a href="{{ __('Logout') }}"><i class="material-icons">input</i>Sign Out</a></li>
                    </ul>
                </div>
            </div>
        </div>
        <!-- #User Info -->
        <!-- Menu -->
        <div class="menu">
            <ul class="list">

                <li class="active">
                    <a href="/admin/dashboard">
                        <i class="material-icons">home</i>
                        <span>Home</span>
                    </a>
                </li>
                @if(auth()->user()->role==1)
                <li>
                    <a href="/admin/users">
                        <i class="material-icons">person_add</i>
                        <span>Users</span>
                    </a>
                </li>
                <li>
                    <a href="/admin/admins">
                        <i class="material-icons">supervisor_account</i>
                        <span>Admins</span>
                    </a>
                </li>
                <li>
                    <a href="/admin/suppliers">
                        <i class="material-icons">person</i>
                        <span>Suppliers</span>
                    </a>
                </li>
                <li>
                    <a href="/admin/places">
                        <i class="material-icons">account_balance</i>
                        <span>Tourist Places</span>
                    </a>
                </li>
                @endif
                @if(auth()->user()->role==2)
                <li>
                    <a href="/admin/services">
                        <i class="material-icons">view_module</i>
                        <span>{{auth()->user()->category->name}}</span>
                    </a>
                </li>
                <li>
                    <a href="/admin/bookings">
                        <i class="material-icons">shopping_cart</i>
                        <span>Bookings</span>
                    </a>
                </li>
                @endif

            </ul>
        </div>
        <!-- #Menu -->
        <!-- Footer -->
        <div class="legal">
            <div class="copyright">
                &copy; 2022 <a href="javascript:void(0);">Stay</a>.
            </div>

        </div>
        <!-- #Footer -->
    </aside>
    <!-- #END# Left Sidebar -->

</section>