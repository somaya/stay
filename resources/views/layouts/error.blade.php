@if(\Illuminate\Support\Facades\Session::has('error'))
    <span class="help-block sweet-alert">
        <strong>{{ \Illuminate\Support\Facades\Session::get('error') }}</strong>
    </span>
@endif