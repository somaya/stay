@extends('admin-layout.app')

@section('content')
    <div class="container-fluid">
        <div class="block-header">
            <h2>DASHBOARD</h2>
        </div>
        @if (auth()->user()->role==1)


        <div class="row clearfix">
            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                <div class="info-box bg-pink hover-expand-effect">
                    <div class="icon">
                        <i class="material-icons">playlist_add_check</i>
                    </div>
                    <div class="content">
                        <div class="text">Services</div>
                        <div class="number count-to" data-from="0" data-to="{{$services}}" data-speed="15" data-fresh-interval="20"></div>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                <div class="info-box bg-cyan hover-expand-effect">
                    <div class="icon">
                        <i class="material-icons">person</i>
                    </div>
                    <div class="content">
                        <div class="text">Suppliers</div>
                        <div class="number count-to" data-from="0" data-to="{{$suppliers}}" data-speed="15" data-fresh-interval="20"></div>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                <div class="info-box bg-orange hover-expand-effect">
                    <div class="icon">
                        <i class="material-icons">person_add</i>
                    </div>
                    <div class="content">
                        <div class="text">Users</div>
                        <div class="number count-to" data-from="0" data-to="{{$users}}" data-speed="15" data-fresh-interval="20"></div>
                    </div>
                </div>
            </div>
            @endif
            @if (auth()->user()->role==2)
            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                <div class="info-box bg-light-green hover-expand-effect">
                    <div class="icon">
                        <i class="material-icons">shopping_cart</i>
                    </div>
                    <div class="content">
                        <div class="text">Bookings</div>
                        <div class="number count-to" data-from="0" data-to="{{$bookings}}" data-speed="15" data-fresh-interval="20"></div>
                    </div>
                </div>
            </div>
            @endif

        </div>

    </div>
@endsection