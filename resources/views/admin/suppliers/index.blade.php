@extends('admin-layout.app')
@section('content')
    @include('message')
    <div class="container-fluid">

        <!-- Basic Examples -->
        <div class="row clearfix">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="card">
                    <div class="header">
                        <h2>
                            Suppliers
                        </h2>

                    </div>
                    <div class="body">
                        <div class="table-responsive">
                            <table class="table table-bordered table-striped table-hover js-basic-example dataTable">
                                <thead>
                                <tr>
                                    <th>Name</th>
                                    <th>Email</th>
                                    <th>Category</th>
                                    <th>Status</th>
                                    <th>Actions</th>
                                    .
                                </tr>
                                </thead>

                                <tbody>
                                @foreach($suppliers as $user)
                                <tr>
                                    <td>{{$user->name}}</td>
                                    <td>{{$user->email}}</td>
                                    <td>{{$user->category->name}}</td>
                                    <td>{{$user->approve?'Approved':'Pending'}}</td>
                                    <td>

                                        <a type="button" title="Edit" href="/admin/suppliers/{{$user->id}}/edit" class="btn btn-warning btn-circle waves-effect waves-circle waves-float">
                                            <i class="material-icons">edit</i>
                                        </a>
                                        @if($user->approve==0)
                                        <a type="button" title="Approve" href="/admin/suppliers/{{$user->id}}/approve" class="btn btn-success btn-circle waves-effect waves-circle waves-float">
                                            <i class="material-icons">done</i>
                                        </a>
                                        <a type="button" title="Reject" href="/admin/suppliers/{{$user->id}}/reject" class="btn btn-danger btn-circle waves-effect waves-circle waves-float">
                                            <i class="material-icons">highlight_off</i>
                                        </a>
                                        @endif
                                        <a type="button" title="Delete" onclick="return false;" object_id="{{ $user->id }}"
                                           delete_url="/admin/suppliers/{{ $user->id }}"
                                            href="#" class="btn btn-danger  delete_user btn-circle waves-effect waves-circle waves-float">
                                            <i class="material-icons">delete</i>
                                        </a>

                                    </td>

                                </tr>
                                @endforeach

                                </tbody>
                            </table>
                            <a href="/admin/suppliers/create">
                                <button type="button" name="button" style="margin: 20px;"
                                        class="btn btn-success pull-right">Add Supplier</button>

                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- #END# Basic Examples -->

    </div>



@endsection