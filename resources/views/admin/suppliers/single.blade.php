@extends('admin-layout.app')
@section('content')

    <div class="row clearfix">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="card">
                <div class="header">
                    <h2>{{isset($user) ?'Edit Supplier':'Add Supplier'}}</h2>

                </div>
                <div class="body">
                    <form id="form_validation_stats" method="POST" action="/admin/suppliers{{ isset($user) ? '/'.$user->id : '' }}">
                        {!! isset($user) ? '<input type="hidden" name="_method" value="PUT">' : '' !!}
                        {{ csrf_field() }}
                        <div class="form-group form-float">
                            <div class="form-line @error('name') focused error @enderror">
                                <input type="text" class="form-control" name="name" value="{{ isset($user) ? $user->name : old('name')}}" >
                                <label class="form-label">Name</label>
                            </div>
                            @error('name')
                            <div class="input-group"><label class="error">{{ $message }}</label></div>
                            @enderror
                        </div>
                        <div class="form-group form-float">
                            <div class="form-line @error('email') focused error @enderror">
                                <input type="text" class="form-control" name="email" value="{{ isset($user) ? $user->email : old('email')}}"  >
                                <label class="form-label">Email</label>
                            </div>
                            @error('email')
                            <div class="input-group"><label class="error">{{ $message }}</label></div>
                            @enderror
                        </div>
                        <div class="form-group form-float">
                            <div class="form-line @error('category') focused error @enderror">
                                <select class="form-control show-tick" name="category">
                                    <option value="">-- Please select --</option>
                                    @foreach($categories as $category)
                                    <option value="{{$category->id}}" {{isset($user) && $user->category->id==$category->id?'selected':''}} {{old('category')==$category->id?'selected':''}}>{{$category->name}}</option>
                                    @endforeach

                                </select>

                                <label class="form-label">Category</label>
                            </div>
                            @error('category')
                            <div class="input-group"><label class="error">{{ $message }}</label></div>
                            @enderror
                        </div>

                        <div class="form-group form-float">
                            <div class="form-line @error('password') focused error @enderror">
                                <input type="password" class="form-control" name="password" >
                                <label class="form-label">Password</label>
                            </div>
                            @error('password')
                            <div class="input-group"><label class="error">{{ $message }}</label></div>
                            @enderror
                        </div>


                        <button class="btn btn-primary waves-effect" type="submit">{{ isset($user) ? 'Edit' : 'Add' }}</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection