@extends('admin-layout.app')
@section('content')
    @include('message')
    <div class="container-fluid">

        <!-- Basic Examples -->
        <div class="row clearfix">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="card">
                    <div class="header">
                        <h2>
                            Users
                        </h2>

                    </div>
                    <div class="body">
                        <div class="table-responsive">
                            <table class="table table-bordered table-striped table-hover js-basic-example dataTable">
                                <thead>
                                <tr>
                                    <th>Name</th>
                                    <th>Email</th>
                                    <th>Actions</th>
                                    .
                                </tr>
                                </thead>

                                <tbody>
                                @foreach($users as $user)
                                <tr>
                                    <td>{{$user->name}}</td>
                                    <td>{{$user->email}}</td>
                                    <td>

                                        <a type="button" title="Edit" href="/admin/users/{{$user->id}}/edit" class="btn btn-warning btn-circle waves-effect waves-circle waves-float">
                                            <i class="material-icons">edit</i>
                                        </a>
                                        <a type="button" title="Delete" onclick="return false;" object_id="{{ $user->id }}"
                                           delete_url="/admin/users/{{ $user->id }}"
                                            href="#" class="btn btn-danger  delete_user btn-circle waves-effect waves-circle waves-float">
                                            <i class="material-icons">delete</i>
                                        </a>

                                    </td>

                                </tr>
                                @endforeach

                                </tbody>
                            </table>
                            <a href="/admin/users/create">
                                <button type="button" name="button" style="margin: 20px;"
                                        class="btn btn-success pull-right">Add User</button>

                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- #END# Basic Examples -->

    </div>



@endsection