@extends('admin-layout.app')
@section('content')

    <div class="row clearfix">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="card">
                <div class="header">
                    <h2>{{isset($place) ?'Edit Place':'Add Place'}}</h2>

                </div>
                <div class="body">
                    <form id="form_validation_stats"   method="POST" action="/admin/places{{ isset($place) ? '/'.$place->id : '' }}" enctype="multipart/form-data">
                        {!! isset($place) ? '<input type="hidden" name="_method" value="PUT">' : '' !!}
                        {{ csrf_field() }}
                        <div class="form-group form-float">
                            <div class="form-line @error('name') focused error @enderror">
                                <input type="text" class="form-control" name="name" value="{{ isset($place) ? $place->name : old('name')}}" >
                                <label class="form-label">Name</label>
                            </div>
                            @error('name')
                            <div class="input-group"><label class="error">{{ $message }}</label></div>
                            @enderror
                        </div>

                        <div class="form-group form-float">
                            <div class="form-line @error('info') focused error @enderror">
                                <textarea  class="form-control" name="info" >{{ isset($place) ? $place->info : old('info')}}</textarea>
                                <label class="form-label">Information</label>
                            </div>
                            @error('info')
                            <div class="input-group"><label class="error">{{ $message }}</label></div>
                            @enderror
                        </div>

                        <div class="form-group form-float">
                            <div class="form-line @error('address') focused error @enderror">
                                <input type="text" class="form-control" name="address" id="address" value="{{ isset($place) ? $place->address : old('address')}}">
                                <div id="map" lat="{{isset($place)? $place->latitude:'28.383509'}}" lng="{{isset($place) ? $place->langitude:'36.566193'}}" style="width: 100%; height: 400px;"></div>
                                <input type="hidden"  id="latitude" name="latitude" value="{{isset($place)? $place->latitude:'28.383509'}}"/>
                                <input type="hidden"  id="langitude" name="langitude"  value="{{isset($place) ? $place->langitude:'36.566193'}}"/>
                                <label class="form-label">Address</label>
                            </div>
                            @error('address')
                            <div class="input-group"><label class="error">{{ $message }}</label></div>
                            @enderror
                        </div>
                        <div class="form-group form-float  ">
                            <div class="form-line @error('images') focused error @enderror">
                                {{--<div class="dz-message">--}}
                                    {{--<div class="drag-icon-cph">--}}
                                        {{--<i class="material-icons">touch_app</i>--}}
                                    {{--</div>--}}
                                    {{--<h3>Drop files here or click to upload.</h3>--}}
                                {{--</div>--}}
                                <div class="fallback">
                                    <input name="images[]" type="file" multiple />
                                </div>


                            </div>
                            @error('images')
                            <div class="input-group"><label class="error">{{ $message }}</label></div>
                            @enderror
                        </div>

                        <div class="row">

                        @if(isset($place) && $place->images->isNotEmpty())
                            @foreach ($place->images as $image)


                                <div class="col-lg-6 col-md-6 col-sm-6" style="margin-bottom: 10px;">
                                    <input type="hidden" name="_token" id="token" value="{{ csrf_token() }}">

                                    <i class="material-icons place-image-trash"
                                       id="{{$image->id}}"
                                       style="position: absolute;top: 6px;right: 30px;z-index: 100;font-size:25px;color:red"
                                       onmousemove="this.style.color='red'"
                                       onmouseout="this.style.color='red'">highlight_off</i>
                                    <img
                                            data-src="holder.js/800x400?auto=yes&amp;bg=777&amp;fg=555&amp;text=First slide"
                                            alt="First slide [800x4a00]"
                                            src="{{$image->image}}"
                                            style="height: 100%; width: 100%"
                                            data-holder-rendered="true">
                                </div>
                        @endforeach
                        @endif
                </div>





                        <button class="btn btn-primary waves-effect" type="submit">{{ isset($place) ? 'Edit' : 'Add' }}</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <script src="{{ Request::root() }}/admin/plugins/jquery/jquery.min.js"></script>
    <script>
        $(document).ready(function () {
            $(".place-image-trash").click(function (e) {
                var $obj = $(this);
//
                $.ajax({
                    url: '/place-image-delete',
                    type: "POST",
                    dataType: 'json',
                    data: {image_id: $obj.attr('id'),_token:_token=$('#token').val()},
                    success: function (data) {
                        if (data == '1') {
                            $obj.parent().remove();
                        }
                    },
                    error: function () {
                    }
                });
            });
        });
    </script>

    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDaFbSOerPJ0NF5IArDBvQ_dX3ODWnln5c&language=en&libraries=places&sensor=true&callback=initMap"
            async defer></script>
    <script>
        $(document).ready(function() {
            $("iframe[name='htmlComp-iframe']").attr("allow", "geolocation");
            $("body")
                .parent()
                .prev()
                .attr("allow", "geolocation");
        });
        var map
        var marker
        var input = document.getElementById('address');
        var infowindow
        var geocoder = new google.maps.Geocoder();
        var autocomplete = new google.maps.places.Autocomplete(input);

        function geolocate() {

            if (navigator.geolocation) {

                navigator.geolocation.getCurrentPosition(function (position) {

                    var  pos = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);

                    marker.setPosition(pos)
                    map.setCenter(pos);
                    geocoder.geocode({'latLng': pos}, function(results, status) {
                        if (status == google.maps.GeocoderStatus.OK) {
                            if (results[0]) {

                                document.getElementById('address').value = results[0].formatted_address;
                                document.getElementById('latitude').value = position.coords.latitude;
                                document.getElementById('langitude').value = position.coords.longitude;
                                infowindow.setContent(results[0].formatted_address);
                                infowindow.open(map, marker);
                            }
                        }
                    });
                });
            }
        }

        function GeolocationControl(controlDiv, map) {

            // Set CSS for the control button
            var controlUI = document.createElement('div');
            controlUI.style.backgroundColor = '#444';
            controlUI.style.borderStyle = 'solid';
            controlUI.style.borderWidth = '1px';
            controlUI.style.borderColor = 'white';
            controlUI.style.height = '28px';
            controlUI.style.marginBottom = '15px';
            controlUI.style.cursor = 'pointer';
            controlUI.style.textAlign = 'center';
            controlUI.title = 'Click to center map on your location';
            controlUI.id = 'goCenterUI';
            controlDiv.appendChild(controlUI);

            // Set CSS for the control text
            var controlText = document.createElement('div');
            controlText.style.fontFamily = 'Arial,sans-serif';
            controlText.style.fontSize = '10px';
            controlText.style.color = 'white';
            controlText.style.paddingLeft = '10px';
            controlText.style.paddingRight = '10px';
            controlText.style.marginTop = '8px';
            controlText.innerHTML = 'Center map on your location';
            controlUI.appendChild(controlText);

            // Setup the click event listeners to geolocate user
            google.maps.event.addDomListener(controlUI, 'click', geolocate);
        }
        google.maps.event.addDomListener(window, "load", initMap);
        function initMap() {


            // infoWindow = new google.maps.InfoWindow;
            infowindow = new google.maps.InfoWindow();
            var m=document.getElementById('map');
            var lng= m.getAttribute('lng');
            var lat= m.getAttribute('lat');
            var latlng = new google.maps.LatLng(lat,lng);
            map = new google.maps.Map(document.getElementById('map'), {
                center: latlng,
                zoom: 13,
                mapTypeControl: false,
                fullscreenControl: false,
                streetViewControl: false

            });
            marker = new google.maps.Marker({
                map: map,
                position: latlng,
                draggable: true,
                anchorPoint: new google.maps.Point(0, -29)
            });


            var geolocationDiv = document.createElement('div');
            var geolocationControl = new GeolocationControl(geolocationDiv, map);
            geolocationDiv.index = 1;
            geolocationDiv.style['padding-top'] = '10px';
            map.controls[google.maps.ControlPosition.BOTTOM_CENTER].push(geolocationDiv);

            input = document.getElementById('address');
            geocoder = new google.maps.Geocoder();
            autocomplete = new google.maps.places.Autocomplete(input);

            map.controls[google.maps.ControlPosition.TOP_CENTER].push(input);


//            if (navigator.geolocation) {
//                console.log("geolocation ok");
//                navigator.geolocation.getCurrentPosition(function(position)
//                {
//                    var pos = {
//                        lat: position.coords.latitude,
//                        lng: position.coords.longitude
//                    };
//                    // infoWindow.setPosition(pos);
//                    // infoWindow.setContent('Location found.');
//                    // infoWindow.open(map);
//                    map.setCenter(pos);
//                    console.log(position.coords.latitude ,position.coords.longitude);
//                    geocoder.geocode({'latLng': pos}, function(results, status) {
//                        if (status == google.maps.GeocoderStatus.OK) {
//                            if (results[0]) {
//                                bindCarDataToForm(results[0].formatted_address,marker.getPosition().lat(),marker.getPosition().lng());
//                                infowindow.setContent(results[0].formatted_address);
//                                infowindow.open(map, marker);
//                            }
//                        }
//                    });
//                    // map.removeMarker(map.markers[0]);
//                    marker.setPosition(pos)
//                    // marker = new google.maps.Marker({
//                    //     position: pos,
//                    //     map: map,
//                    //     draggable:true,
//                    //     animation: google.maps.Animation.DROP,
//                    // });
//                    // marker.setMap(map);
//                }, function() {
//                    handleLocationError(true, infoWindow, map.getCenter());
//                });
//            }else {
//                // Browser doesn't support Geolocation
//                handleLocationError(false, infoWindow, map.getCenter());
//            }
            function handleLocationError(browserHasGeolocation, infoWindow, pos) {
                infoWindow.setPosition(pos);
                infoWindow.setContent(browserHasGeolocation ?
                    'Error: The Geolocation place failed.' :
                    'Error: Your browser doesn\'t support geolocation.');
                infoWindow.open(map);
            }
            autocomplete.bindTo('bounds', map);

            autocomplete.addListener('place_changed', function() {
                infowindow.close();
                marker.setVisible(false);
                var place = autocomplete.getPlace();
                if (!place.geometry) {
                    window.alert("Autocomplete's returned place contains no geometry");
                    return;
                }

                // If the place has a geometry, then present it on a map.
                if (place.geometry.viewport) {
                    map.fitBounds(place.geometry.viewport);
                } else {
                    map.setCenter(place.geometry.location);
//                    map.setZoom(13);
                }

                marker.setPosition(place.geometry.location);
                marker.setVisible(true);

                bindCarDataToForm(place.formatted_address,place.geometry.location.lat(),place.geometry.location.lng());
                infowindow.setContent(place.formatted_address);
                infowindow.open(map, marker);

            });
            // this function will work on marker move event into map
            google.maps.event.addListener(marker, 'dragend', function() {
                geocoder.geocode({'latLng': marker.getPosition()}, function(results, status) {
                    if (status == google.maps.GeocoderStatus.OK) {
                        if (results[0]) {
                            bindCarDataToForm(results[0].formatted_address,marker.getPosition().lat(),marker.getPosition().lng());
                            infowindow.setContent(results[0].formatted_address);
                            infowindow.open(map, marker);
                        }
                    }
                });
            });

        }
        function bindCarDataToForm(address,lat,lng){
            document.getElementById('address').value = address;
            document.getElementById('latitude').value = lat;
            document.getElementById('langitude').value = lng;
        }



    </script>
@endsection
