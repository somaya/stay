@extends('admin-layout.app')
@section('content')
    @include('message')
    <div class="container-fluid">

        <!-- Basic Examples -->
        <div class="row clearfix">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="card">
                    <div class="header">
                        <h2>
                            Bookings
                        </h2>

                    </div>
                    <div class="body">
                        <div class="table-responsive">
                            <table class="table table-bordered table-striped table-hover js-basic-example dataTable">
                                <thead>
                                <tr>
                                    <th>Service Name</th>
                                    <th>Date</th>
                                    <th>User</th>
                                    <th>Actions</th>
                                    .
                                </tr>
                                </thead>

                                <tbody>
                                @foreach($bookings as $booking)
                                <tr>
                                    <td>{{$booking->service->name}}</td>
                                    <td>{{$booking->date}}</td>
                                    <td>{{$booking->user->name}}</td>
                                    <td>

                                        <a type="button" title="Show" href="/admin/bookings/{{$booking->id}}" class="btn btn-success btn-circle waves-effect waves-circle waves-float">
                                            <i class="material-icons">visibility</i>
                                        </a>


                                    </td>

                                </tr>
                                @endforeach

                                </tbody>
                            </table>

                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- #END# Basic Examples -->

    </div>



@endsection