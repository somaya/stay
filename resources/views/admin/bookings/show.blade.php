@extends('admin-layout.app')
@section('content')

    <div class="row clearfix">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="card">
                <div class="header">
                    <h2>Booking Details</h2>

                </div>
                <div class="body">

                        <div class="form-group form-float">
                            <div class="form-line ">
                                <input type="text" class="form-control"  value="{{$booking->service->name}}" disabled >
                                <label class="form-label">Service Name</label>
                            </div>

                        </div>
                        <div class="form-group form-float">
                            <div class="form-line ">
                                <input type="text" class="form-control"  value="{{ $booking->date}}" disabled >
                                <label class="form-label">Date</label>
                            </div>

                        </div>
                    @if(in_array($booking->service->category_id, [1,3,4]))
                        <div class="form-group form-float">
                            <div class="form-line ">
                                <input type="text" class="form-control"  value="{{ $booking->time}}" disabled >
                                <label class="form-label">Time</label>
                            </div>

                        </div>
                    @endif
                        <div class="form-group form-float">
                            <div class="form-line ">
                                <input type="text" class="form-control"  value="{{ $booking->service->price}}" disabled >
                                <label class="form-label">Price</label>
                            </div>

                        </div>
                     @if($booking->service->category_id==1)
                        <div class="form-group form-float">
                            <div class="form-line ">
                                <input type="text" class="form-control"  value="{{ $booking->delivary?'Yes':'No'}}" disabled >
                                <label class="form-label">Delivary</label>
                            </div>

                        </div>
                        <div class="form-group form-float">
                            <div class="form-line ">
                                <input type="text" class="form-control"  value="{{ $booking->service->meals}}" disabled >
                                <label class="form-label">Meals</label>
                            </div>

                        </div>
                     @endif
                    @if($booking->service->category_id==2)
                        <div class="form-group form-float">
                            <div class="form-line ">
                                <input type="text" class="form-control"  value="{{$booking->service->room_type}}" disabled >
                                <label class="form-label">Room Type</label>
                            </div>

                        </div>
                    @endif
                    @if($booking->service->category_id==4)
                        <div class="form-group form-float">
                            <div class="form-line ">
                                <input type="text" class="form-control"  value="{{$booking->service->car_type}}" disabled >
                                <label class="form-label">Car Type</label>
                            </div>

                        </div>
                    @endif
                    @if($booking->service->category_id==3)
                        <div class="form-group form-float">
                            <div class="form-line ">
                                <input type="text" class="form-control"  value="{{$booking->service->speciality}}" disabled >
                                <label class="form-label">Speciality</label>
                            </div>

                        </div>
                    @endif


                </div>
            </div>
        </div>
    </div>
@endsection