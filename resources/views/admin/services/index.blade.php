@extends('admin-layout.app')
@section('content')
    @include('message')
    <div class="container-fluid">

        <!-- Basic Examples -->
        <div class="row clearfix">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="card">
                    <div class="header">
                        <h2>
                            {{auth()->user()->category->name}}                        </h2>

                    </div>
                    <div class="body">
                        <div class="table-responsive">
                            <table class="table table-bordered table-striped table-hover js-basic-example dataTable">
                                <thead>
                                <tr>
                                    <th>Name</th>
                                    <th>Phone</th>
                                    <th>Address</th>
                                    <th>price</th>
                                    <th>Actions</th>
                                    .
                                </tr>
                                </thead>

                                <tbody>
                                @foreach($services as $service)
                                <tr>
                                    <td>{{$service->name}}</td>
                                    <td>{{$service->phone}}</td>
                                    <td>{{$service->address}}</td>
                                    <td>{{$service->price}}</td>
                                    <td>

                                        <a type="button" title="Edit" href="/admin/services/{{$service->id}}/edit" class="btn btn-warning btn-circle waves-effect waves-circle waves-float">
                                            <i class="material-icons">edit</i>
                                        </a>

                                        <a type="button" title="Delete" onclick="return false;" object_id="{{ $service->id }}"
                                           delete_url="/admin/services/{{ $service->id }}"
                                            href="#" class="btn btn-danger  delete_user btn-circle waves-effect waves-circle waves-float">
                                            <i class="material-icons">delete</i>
                                        </a>

                                    </td>

                                </tr>
                                @endforeach

                                </tbody>
                            </table>
                            <a href="/admin/services/create">
                                <button type="button" name="button" style="margin: 20px;"
                                        class="btn btn-success pull-right">Add New</button>

                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- #END# Basic Examples -->

    </div>



@endsection