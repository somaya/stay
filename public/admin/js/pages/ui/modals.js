$(function () {
    $('.js-modal-buttons .btn').on('click', function () {
        var color = $(this).data('color');
        $('#mdModal .modal-content').removeAttr('class').addClass('modal-content modal-col-' + color);
        $('#mdModal').modal('show');
    });



});
$(document).ready(function () {
    $('.delete_user').click(function () {
        console.log('enter');
           $.ajaxSetup({
               headers: {
                   'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
               }
           });

           var id = $(this).attr('object_id');
           var d_url = $(this).attr('delete_url');
           var elem = $(this).parent('td').parent('tr');
           var token = $('meta[name="_token"]').attr('content');
           $.ajax({
               url : d_url,
               type : "delete",
               data : {id:id},
               success : function(result) {
                   elem.hide(1000);

               }
           });

    });

});

