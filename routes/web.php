<?php

use App\Http\Controllers\PlacesController;
use App\Http\Controllers\ServicesController;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('website.home');
});
Route::post('/register/supplier', 'Auth\RegisterController@regiter_supplier');
Auth::routes();
Route::post('/image-delete', 'admin\ServicesController@image_delete');
Route::post('/place-image-delete', 'admin\PlacesController@image_delete');

Route::group(['prefix' => 'admin', 'middleware' => ['web', 'admin']], function () {
    Route::get('/dashboard', 'DashboardController@index');
    Route::resource('/users', 'admin\UsersController');
    Route::resource('/admins', 'admin\AdminsController');
    Route::resource('/suppliers', 'admin\SuppliersController');
    Route::resource('/services', 'admin\ServicesController');
    Route::resource('/places', 'admin\PlacesController');
    Route::get('/suppliers/{id}/approve', 'admin\SuppliersController@approve');
    Route::get('/suppliers/{id}/reject', 'admin\SuppliersController@reject');
    Route::get('/Logout', 'Auth\LoginController@logout');
    Route::resource('/bookings', 'admin\BookingsController');


});


Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
Route::get('/services/{name}', [ServicesController::class, 'index']);
Route::get('/services/{name}/{id}', [ServicesController::class, 'show']);
Route::get('/places', [PlacesController::class, 'index']);
Route::get('/places/{id}', [PlacesController::class, 'show']);
Route::view('/about', 'website.about');

Route::group(['middleware' => ['auth']], function () {
    Route::get('/booking/{name}/{id}', [ServicesController::class, 'showBookingForm']);
    Route::post('/booking/{name}/{id}', [ServicesController::class, 'storeBooking']);
});
